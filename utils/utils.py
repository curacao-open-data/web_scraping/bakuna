import re
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def regex_search(string, result):
    regex = r"\>([\w áíéúó]+)\<[\w\W]+count[\W]+([0-9]+\.{0,2}[0-9]+)\<"
    matches = re.finditer(regex, string, re.MULTILINE)
    for matchNum, match in enumerate(matches, start=1):
        result[match.group(1)] = [int(str(match.group(2)).replace(".", ""))]
    return result


def get_html(url):
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--incognito')
    options.add_argument('--headless')
    with webdriver.Chrome("driver\chromedriver.exe", options=options) as driver:
        driver.get(url)
        return driver.page_source
