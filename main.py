# Importing Libraries
import pandas as pd

from requests import get
from bs4 import BeautifulSoup
from datetime import datetime as datetime
from utils.utils import get_html, regex_search


def main(url, excel_output_dir, csv_output_dir):
    html_soup = BeautifulSoup(get_html(url), 'lxml')
    Stats_container = html_soup.find_all('div', class_='stats')
    result = {}
    for line in Stats_container:
        result = regex_search(str(line), result)
    result['execution_time'] = str(datetime.today())
    date = str(datetime.today().date())
    df_i = pd.DataFrame.from_dict(result, orient='columns', dtype=None)
    try:
        df_a = pd.read_excel(excel_output_dir)
        df_i = pd.concat([df_a, df_i])
    except FileNotFoundError as E:
        print(repr(E))
        pass
    df_i.to_excel(excel_output_dir, index=False)
    df_i.to_csv(csv_output_dir, mode="w", header=True, index=False)


if __name__ == "__main__":
    url = "https://bakuna-counter.ibis-management.com/"
    excel_output_dir = "data/export.xlsx"
    csv_output_dir = "data/backup/export.csv"
    main(url, excel_output_dir, csv_output_dir)
