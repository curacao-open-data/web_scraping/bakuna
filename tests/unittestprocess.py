import unittest

import utils.utils as ut


class TestCase(unittest.TestCase):
    def test_regex_search(self):
        test_string = '<div class="stats"><p class="label">Total di bakuna apliká</p><p class="count">80.350</p></div>'
        result = ut.regex_search(test_string, {})
        self.assertEqual(80350, result["Total di bakuna apliká"][0])
